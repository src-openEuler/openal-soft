Name:           openal-soft
Version:        1.24.2
Release:        1
Summary:        A software implementation of the OpenAL 3D audio API
License:        LGPL-2.0-or-later AND BSD-3-Clause
URL:            https://openal-soft.org/
Source0:        https://openal-soft.org/openal-releases/openal-soft-%{version}.tar.bz2
BuildRequires:  alsa-lib-devel cmake fluidsynth-devel portaudio-devel pulseaudio-libs-devel
BuildRequires:  cmake(Qt5Widgets) SDL2-devel
BuildRequires:  pkgconfig(libpipewire-0.3) >= 0.3.23
BuildRequires:  libmysofa-devel libsndfile-devel gcc gcc-c++
Obsoletes:      openal <= 0.0.10
Provides:       openal = %{version}

%description
OpenAL Soft is an LGPL-licensed, cross-platform, software implementation of the OpenAL 3D
audio API. It's forked from the open-sourced Windows version available originally from
openal.org's SVN repository (now defunct).
OpenAL provides capabilities for playing audio in a virtual 3D environment. Distance attenuation,
doppler shift, and directional sound emitters are among the features handled by the API.
More advanced effects, including air absorption, occlusion, and environmental reverb, are available
through the EFX extension. It also facilitates streaming audio, multi-channel buffers, and audio capture.

%package        devel
Summary:        Development package for %{name}
Requires:       %{name} = %{version}-%{release}
Obsoletes:      openal-devel <= 0.0.10 %{name}-examples < %{version}-%{release}
Provides:       openal-devel = %{version} %{name}-examples = %{version}-%{release}

%description    devel
This package contains some libraries and header files for the development
use of %{name}. It also contains some sample applications for %{name}.

%package        qt
Summary:        Qt frontend for configuring %{name}
Requires:       %{name} = %{version}-%{release}

%description    qt
This package contains a Qt-based tool, alsoft-config,
for configuring %{name} features.

%prep
%autosetup -p1

%build
%cmake -DALSOFT_CPUEXT_NEON:BOOL=OFF
%cmake_build

%install
%cmake_install

install -Dpm644 alsoftrc.sample %{buildroot}%{_sysconfdir}/openal/alsoft.conf
sed -i 's/#allow-moves = false/allow-moves = true/' %{buildroot}%{_sysconfdir}/openal/alsoft.conf

%files
%license COPYING
%{_libdir}/libopenal.so.*
%{_bindir}/openal-info
%dir %{_sysconfdir}/openal
%config(noreplace) %{_sysconfdir}/openal/alsoft.conf
%{_datadir}/openal
%exclude %{_datadir}/openal/alsoftrc.sample
%exclude %{_datadir}/openal/presets/presets.txt

%files devel
%{_bindir}/{makemhr,alhrtf,allatency,almultireverb,alplay}
%{_bindir}/{alloopback,alrecord,alreverb,alstream,altonegen}
%{_bindir}/{aldebug,aldirect,alffplay,allafplay}
%{_includedir}/*
%{_libdir}/libopenal.so
%{_libdir}/pkgconfig/openal.pc
%{_libdir}/cmake/OpenAL

%files qt
%{_bindir}/alsoft-config

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1.24.2-1
- update to 1.24.2

* Thu Nov 28 2024 Funda Wang <fundawang@yeah.net> - 1.24.1-1
- update to 1.24.1

* Wed Nov 06 2024 Funda Wang <fundawang@yeah.net> - 1.23.1-2
- adopt to new cmake macro

* Fri Mar 15 2024 yaoxin <yao_xin001@hoperun.com> - 1.23.1-1
- Upgrade to 1.23.1

* Sat Nov 16 2019 huzhiyu <huzhiyu1@huawei.com> - 1.18.2-7
- Package init
